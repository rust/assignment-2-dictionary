section .text

%define SYS_READ 0
%define SYS_WRITE 1
%define SYS_EXIT 60

%define STDOUT 1
%define STDERR 2
 
; Принимает код возврата и завершает текущий процесс
global exit
exit: 
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
global string_length
string_length:
    xor rax, rax

.loop:
    inc rax
    cmp byte [rdi+rax-1], 0
    jne .loop

    dec rax
    ret
    
%macro print 1
    push rdi
    call string_length
    pop rsi      ; buf
    mov rdx, rax ; len
    mov rdi, %1
    mov rax, SYS_WRITE
    syscall
%endmacro

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
global print_string
print_string:
    print STDOUT
    ret

global print_string_stderr
print_string_stderr:
    print STDERR
    ret

; Принимает код символа и выводит его в stdout
global print_char
print_char:
    push rdi
    mov rdx, 1   ; len
    mov rsi, rsp ; buf
    mov rdi, STDOUT
    mov rax, SYS_WRITE
    syscall
    pop rdi
    ret
    
; Переводит строку (выводит символ с кодом 0xA)
global print_newline
print_newline:
    mov dil, `\n`
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
global print_uint
print_uint:
    or rax, rax
    jnz .nonzero
    mov rdi, '0'
    jmp print_char

.nonzero:
    sub rsp, 32
    mov [rsp+24], rbx

    mov rax, rdi
    mov rcx, 10
    mov rbx, 22
    mov byte [rsp+23], 0

.loop:
    xor rdx, rdx
    div rcx
    add dl, '0'
    mov [rsp+rbx], dl
    dec rbx
    or rax, rax
    jnz .loop

    lea rdi, [rsp+rbx+1]
    call print_string
    
    mov rbx, [rsp+24]
    add rsp, 32
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
global print_int
print_int:
    or rdi, rdi
    js .negative
    jmp print_uint
.negative:
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
global string_equals
string_equals:
    xor rcx, rcx

.loop:
    mov al, [rdi+rcx]
    cmp al, [rsi+rcx]
    jne .neq
    inc rcx
    or al, al
    jnz .loop
    
    mov rax, 1
    ret
.neq:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
global read_char
read_char:
    push 0
    mov rsi, rsp ; buf
    mov rdx, 1   ; len
    xor rdi, rdi ; stdin
    mov rax, SYS_READ
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
global read_word
read_word:
    push rbx
    push r12
    push r13
    xor rbx, rbx
    mov r12, rdi
    mov r13, rsi
    dec r13    ; место под нуль-терминатор
    
    jmp .loop

.ws:
    or rbx, rbx
    jnz .retword

.loop:
    call read_char
    or al, al
    jz .retword

    cmp al, ' '
    je .ws
    cmp al, `\n`
    je .ws
    cmp al, `\t`
    je .ws
    
    inc rbx
    cmp rbx, r13
    ja .fail

    mov [r12+rbx-1], al
    jmp .loop

.retword:
    mov byte [r12+rbx], 0
    mov rax, r12
    mov rdx, rbx
    jmp .ret
.fail:
    xor rax, rax
.ret:
    pop r13
    pop r12
    pop rbx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
global print_uint
parse_uint:
    xor rax, rax
    xor rdx, rdx
.loop:
    movzx r8, byte [rdi+rdx]
    sub r8b, '0'
    jc .break
    cmp r8b, 9
    ja .break
    inc rdx
    add rax, rax
    lea rax, [rax+rax*4]
    add rax, r8
    jmp .loop
.break:
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
global print_int
parse_int:
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
global string_copy
string_copy:
    xor rax, rax
.loop:
    mov cl, [rdi+rax]
    dec rdx
    js .overflow
    mov [rsi+rax], cl
    test cl, cl
    jz .ret
    inc rax
    jmp .loop
.overflow:
    xor rax, rax
.ret:
    ret

