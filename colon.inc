%define LIST_START 0

%macro colon 2
  %ifnstr %1
    %error "colon: string literal expected as first argument"
  %endif
  %ifnid %2
    %error "colon: identifier expected as second argument"
  %endif
  
  %2: dq LIST_START 
  %define LIST_START %2
  db %1, 0
%endmacro
