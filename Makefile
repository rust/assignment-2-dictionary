BIN=main

AS=nasm
ASFLAGS=-g -felf64

SOURCES=$(wildcard *.asm)
OBJECTS=$(SOURCES:.asm=.o)

.PHONY: all clean
all: $(BIN)

clean:
	rm -f $(OBJECTS)

$(BIN): $(OBJECTS)
	$(LD) $(LDFLAGS) $^ -o $(BIN)
	
%.o: %.asm
	$(AS) $(ASFLAGS) $< -o $@