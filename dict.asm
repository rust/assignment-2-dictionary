%include "lib.inc"

%define DICT_KEY_OFFSET 8

section .text

global find_word
find_word:
	push rbx
	mov rbx, rsi
	
.loop:
	lea rsi, [rbx+DICT_KEY_OFFSET]
    call string_equals
	test rax, rax
	jnz .found
	mov rbx, [rbx]
	test rbx, rbx
	jz .not_found
	jmp .loop

.found:
	mov rax, rbx
	pop rbx
	ret

.not_found:
	xor rax, rax
	pop rbx
	ret

