%include "lib.inc"
%include "dict.inc"

section .bss

%define BUF_SIZE 256
buf: resb BUF_SIZE

section .rodata

error_msg_key_not_found:
	db "Key not found!", `\n\0`
error_msg_key_too_long:
	db "Key too long!", `\n\0`

%include "words.inc"

section .text

global _start
_start:
	mov rdi, buf
	mov rsi, BUF_SIZE
	call read_word
	test rax, rax
	jz .error_key_too_long
	
	mov rdi, rax
	mov rsi, LIST_START
	call find_word
	test rax, rax
	jz .error_key_not_found
	
	lea rdi, [rax+DICT_KEY_OFFSET]
	push rdi
	call string_length
	pop rdi

	lea rdi, [rdi+rax+1]
	call print_string
	call print_newline

	xor rdi, rdi
	jmp exit
	
.error_key_not_found:
	mov rdi, error_msg_key_not_found
	jmp .error
.error_key_too_long:
	mov rdi, error_msg_key_too_long
.error:
	call print_string_stderr
	mov rdi, 1
	jmp exit
